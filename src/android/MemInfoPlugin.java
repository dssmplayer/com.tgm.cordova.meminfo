package com.tgm.cordova.MemInfo;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

//import android.content.Context;
//import android.net.wifi.WifiManager;
//import java.net.NetworkInterface;
//import java.util.Collections;
//import java.util.List;
import android.os.Debug;

/**
 * The Class MemInfoPlugin.
 */

public class MemInfoPlugin extends CordovaPlugin {

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.cordova.api.Plugin#execute(java.lang.String,
     * org.json.JSONArray, java.lang.String)
     */
	@Override
	public boolean execute(String action, JSONArray args,
			CallbackContext callbackContext) {

		if(action.equals("getMemInfo")){
			long max = Runtime.getRuntime().maxMemory();
			long used = Debug.getNativeHeapAllocatedSize();

			JSONObject JSONresult = new JSONObject();
			try {
				JSONresult.put("total", Long.toString(max));
				JSONresult.put("heapAlloc", Long.toString(used));

				PluginResult r = new PluginResult(PluginResult.Status.OK,
						JSONresult);
				callbackContext.success(Long.toString(used));
				r.setKeepCallback(true);
				callbackContext.sendPluginResult(r);
				return true;
			} catch (JSONException jsonEx) {
				PluginResult r = new PluginResult(
						PluginResult.Status.JSON_EXCEPTION);
				callbackContext.error("error");
				r.setKeepCallback(true);
				callbackContext.sendPluginResult(r);
				return true;
			}
		}else if(action.equals("exitApp")){
			//cordova.getActivity().finish();
			cordova.getActivity().finishAffinity();
			System.runFinalizersOnExit(true);
			System.exit(0);
			return true;
		}
		return false;
	}
}
